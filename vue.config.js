"use strict"
const path = require('path')
const fs = require('fs')
function resolve(dir) {
  return path.join(__dirname, dir)
}
function base64_encode(file) {
  let bitmap = fs.readFileSync(file)
  return 'data:text/css;base64,' + new Buffer(bitmap).toString('base64')
}
console.log("==========", base64_encode('./src/styles/code.css'))
const name = ""
module.exports = {
  publicPath: '/',
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        "@": resolve("src")
      }
    }
  },
  /**
   * 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
   *  打包之后发现map文件过大，项目文件体积很大，设置为false就可以不输出map文件
   *  map文件的作用在于：项目打包后，代码都是经过压缩加密的，如果运行时报错，输出的错误信息无法准确得知是哪里的代码报错。
   *  有了map就可以像未加密的代码一样，准确的输出是哪一行哪一列有错。
   * */
  productionSourceMap: false,
  // 用于放置生成的静态资源 (js、css、img、fonts) 的；（项目打包之后，静态资源会放在这个文件夹下)
  assetsDir: 'static'
}
