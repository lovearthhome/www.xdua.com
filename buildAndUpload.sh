rm -rf dist/*
npm run build

cd dist
for element in `ls .`
do
    if [ $element == "avatars2" ];then
        echo "skip " $element
        continue
    fi
    echo $element
    ossutil cp -rf $element "oss://www-xdua-com/"$element
done
cd ..
