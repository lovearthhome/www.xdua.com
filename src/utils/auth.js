export function getToken() {
  return sessionStorage.getItem("token") // Cookies.get(TokenKey);
}
export function setToken(token) {
  return sessionStorage.setItem("token", token) // Cookies.set(TokenKey, token);
}
export function removeToken() {
  return sessionStorage.removeItem("token")
}
