import Vue from 'vue'
import { Loading, MessageBox, Tabs, Scrollbar, TabPane, Form, Col, FormItem, DatePicker, Message, Notification, ButtonGroup, Card, Button, Input, Table, TableColumn, Pagination } from 'element-ui'

Vue.use(Loading.directive)
Vue.use(ButtonGroup)
Vue.use(Card)
Vue.use(Button)
Vue.use(Input)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Pagination)
Vue.use(DatePicker)
Vue.use(Form)
Vue.use(Col)
Vue.use(FormItem)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Scrollbar)

Vue.prototype.$loading = Loading.service
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$notify = Notification
Vue.prototype.$message = Message
