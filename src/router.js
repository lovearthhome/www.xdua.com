import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'
import { env } from '@/core/js/env'
import MainComponent from '@/lazyout/Main.vue'

let verifyLogin = (to, from, next) => {
  let uid = env.getUser().uid
  if (uid) {
    let vm = env.getVm()
    // next()
    vm.$dua.isLogin().then(res => {
      if (res) {
        next()
      } else {
        env.setUser({ uid: '' })
        router.push({ name: 'login' })
      }
    })
  } else {
    // next()
    router.push({ name: 'login' })
  }
}

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
Vue.use(Router)

const router = new Router({
  mode: 'hash',
  // base: process.env.BASE_URL,
  base: '/',
  routes: [
    {
      path: '*',
      hidden: true,
      redirect: '/passport'
      // component: () => import('@/views/not-found/NotFound.vue')
    },
    {
      path: '/zone',
      name: 'zone',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'zone-list',
          meta: { title: '账户管理', icon: 'el-icon-menu' },
          component: () => import('@/views/zone/Zone.vue')
        }
      ]
    },
    {
      path: '/group',
      name: 'group',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'group-list',
          meta: { title: '我的社区', icon: 'el-icon-document' },
          component: () => import('@/views/group/list/Group.vue')
        }
      ]
    },
    {
      path: '/device-password',
      name: 'device-password',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'device-password-list',
          meta: { title: '我的社区', icon: 'el-icon-document' },
          component: () => import('@/views/device-password/index')
        }
      ]
    },
    {
      path: '/role/:ugrp_id',
      name: 'role',
      component: MainComponent,
      hidden: true,
      children: [
        {
          path: '',
          name: 'role-list',
          component: () => import('@/views/group/role/Role.vue')
        }
      ]
    },
    {
      path: '/usro/:role_id',
      name: 'usro',
      component: MainComponent,
      hidden: true,
      children: [
        {
          path: '',
          name: 'usro-list',
          component: () => import('@/views/group/auth/Auth.vue')
        }
      ]
    },
    {
      path: '/apm',
      name: 'apm',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'apm-list',
          meta: { title: '我的应用', icon: 'el-icon-setting' },
          component: () => import('@/views/apm/list/Apm.vue')
        }
      ]
    },
    {
      path: '/sleep',
      name: 'sleep',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'sleep-list',
          meta: { title: '我的睡眠', icon: 'el-icon-document' },
          component: () => import('@/views/sleep/list.vue')
        }
      ]
    },
    {
      path: '/belt',
      name: 'belt',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'belt-list',
          meta: { title: '我的设备', icon: 'el-icon-document' },
          component: () => import('@/views/belt/list.vue')
        }
      ]
    },
    {
      path: '/products',
      name: 'products',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'products-list',
          meta: { title: '我的产品', icon: 'el-icon-document' },
          component: () => import('@/views/products/list.vue')
        }
      ]
    },
    {
      path: '/object',
      name: 'obj',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'obj-list',
          meta: { title: '我的对象', icon: 'el-icon-document' },
          component: () => import('@/views/object/list.vue')
        }
      ]
    },
    {
      path: '/deve',
      name: 'deve',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'dev-list',
          meta: { title: '睡眠异常', icon: 'el-icon-document' },
          component: () => import('@/views/deve/list.vue')
        }
      ]
    },
    {
      path: '/sleep-chart',
      name: 'sleep-chart',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'sleep-chart-list',
          meta: { title: '绵绵床带', icon: 'el-icon-document' },
          component: () => import('@/views/sleep-chart/list.vue')
        }
      ]
    },
    {
      path: '/bluetooth-gateway',
      name: 'bluetooth-gateway',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'bluetooth-gateway-list',
          meta: { title: '蓝牙网关', icon: 'el-icon-document' },
          component: () => import('@/views/bluetooth-gateway/list.vue')
        }
      ]
    },
    {
      path: '/iBeacon',
      name: 'iBeacon',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'iBeacon-list',
          meta: { title: 'iBeacon', icon: 'el-icon-document' },
          component: () => import('@/views/iBeacon/list.vue')
        }
      ]
    },
    {
      path: '/blood-oxygen',
      name: 'blood-oxygen',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'blood-oxygen-list',
          meta: { title: 'blood-oxygen', icon: 'el-icon-document' },
          component: () => import('@/views/blood-oxygen/list.vue')
        }
      ]
    },
    {
      path: '/oxygen-chart',
      name: 'oxygen-chart',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'oxygen-chart-list',
          meta: { title: 'oxygen-chart', icon: 'el-icon-document' },
          component: () => import('@/views/oxygen-chart/list.vue')
        }
      ]
    },
    {
      path: '/enviroment',
      name: 'enviroment',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'enviroment-list',
          meta: { title: 'enviroment', icon: 'el-icon-document' },
          component: () => import('@/views/enviroment/list.vue')
        }
      ]
    },
    {
      path: '/devl',
      name: 'devl',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'devl-list',
          meta: { title: '睡眠日志', icon: 'el-icon-document' },
          component: () => import('@/views/devl/list.vue')
        }
      ]
    },
    {
      path: '/user',
      name: 'user',
      component: MainComponent,
      hidden: true,
      children: [
        {
          path: '',
          name: 'user-list',
          component: () => import('@/views/user/User.vue')
        }
      ]
    },

    {
      path: '/passport',
      component: () => import('@/lazyout/Passport.vue'),
      hidden: true,
      children: [
        {
          path: '',
          name: '',
          redirect: { name: 'login' }
        },
        {
          path: 'login',
          name: 'login',
          component: () => import('@/views/passport/Login.vue')
        },
        {
          path: 'register',
          name: 'register',
          component: () => import('@/views/passport/Register.vue')
        },
        {
          path: 'contact',
          name: 'contact',
          component: () => import('@/views/passport/Contact.vue')
        }
      ]
    },
    {
      path: '/data',
      name: 'data',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'data-list',
          meta: { title: '我的数据', icon: 'el-icon-date' },
          component: () => import('@/views/data/list.vue')
        }
      ]
    },
    {
      path: '/file',
      name: 'file',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'file-list',
          meta: { title: '我的文件', icon: 'el-icon-document' },
          component: () => import('@/views/file/list.vue')
        }
      ]
    },
    {
      path: '/equipment',
      name: 'equipment-list',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'equipment-list',
          meta: { title: '我的设备', icon: 'el-icon-printer' },
          component: () => import('@/views/equipment/list.vue')
        }
      ]
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
    {
      path: '/gwlog',
      name: 'gwlog-list',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'gwlog-list',
          meta: { title: '网关日志', icon: 'el-icon-setting' },
          component: () => import('@/views/gwlog/list.vue')
        }
      ]
    },
    {
      path: '/dvlog',
      name: 'dvlog-list',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'dvlog-list',
          meta: { title: '设备日志', icon: 'el-icon-printer' },
          component: () => import('@/views/dvlog/list.vue')
        }
      ]
    },
    {
      path: '/gwenv',
      name: 'gwenv-list',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'gwenv-list',
          meta: { title: '网管环境', icon: 'el-icon-printer' },
          component: () => import('@/views/gwenv/list.vue')
        }
      ]
    },
    {
      path: '/heat-map',
      name: 'heat-map',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'heat-map-list',
          meta: { title: '热图界面', icon: 'el-icon-printer' },
          component: () => import('@/views/heat-map/list.vue')
        }
      ]
    },
    {
      path: '/iotpass',
      name: 'iotpass-list',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'iotpass-list',
          meta: { title: '网关演示', icon: 'el-icon-printer' },
          component: () => import('@/views/iotpass/list.vue')
        }
      ]
    },
    {
      path: '/device',
      name: 'device-list',
      component: MainComponent,
      children: [
        {
          path: '',
          name: 'device-list',
          meta: { title: '设备密码', icon: 'el-icon-printer' },
          component: () => import('@/views/device/list.vue')
        }
      ]
    }
  ]
})

export default router
