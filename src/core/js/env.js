import Vue from 'vue'

class Env {
  constructor() {
    this.user = {
      name: '',
      userName: '',
      uid: ''
    }
    this.vm = new Vue()
  }

  init() {
    this.initUser()
  }

  initUser() {
    let userCache = sessionStorage.getItem('user')
    if (userCache && userCache !== null) {
      this.user = JSON.parse(userCache)
    }
  }

  getUser() {
    return this.user
  }

  setUser(user) {
    this.user = Object.assign({}, this.user, user)
    sessionStorage.setItem('user', JSON.stringify(this.user))
  }

  getVm() {
    return this.vm
  }

  setVm(vm) {
    this.vm = vm
  }
}

let env = new Env()
export { env }
