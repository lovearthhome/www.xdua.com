/**
 * 错误处理方式
 */
function errorDispose(vm, res, message) {
  if (res) {
    console.log(res)
    let status = res.error
    if (status === 0) {
      // 业务成功
      return false
    } else if (status !== 0) {
      console.log(res)
      let msg = message || res.reason.message || '出错了'
      if (res.error === 40101 && res.reason === 'AuthroizationExpired') {
        vm.$router.push('login')
      } else {
        vm.$message.error(msg)
      }
      // let msg = message || res.result.tip || '出错了'
      return true
    }
    // 新增业务处理方式 根据状态跳转登录
  }
}

/**
 * 通用错误处理 减少try catch使用
 * @param {Object} vm this对象
 * @param {string} message 提示信息
 * @return <Function> => <Boolean>
 */
export default function(vm, res, message) {
  // return function(res) {
  return errorDispose(vm, res, message)
  // }
}
