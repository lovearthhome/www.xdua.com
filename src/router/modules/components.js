/** When your routing table is too long, you can split it into small modules **/

import MainComponent from '@/lazyout/ZoneMain.vue'

export const ComponentsRouter = [
  {
    path: '/test/room',
    name: 'room',
    component: MainComponent,
    children: [
      {
        path: '/test/room',
        name: 'room',
        meta: { title: '我的房间', icon: 'el-icon-menu' },
        component: () => import('@/views/room/list.vue')
      }
    ]
  },
  {
    path: '/test/usrob',
    name: 'usrob',
    component: MainComponent,
    children: [
      {
        path: '/test/usrob',
        name: 'usrob',
        meta: { title: '人员名单', icon: 'el-icon-menu' },
        component: () => import('@/views/usrob/list.vue')
      }
    ]
  }
]

