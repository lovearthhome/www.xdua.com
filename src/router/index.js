import Vue from 'vue'
import Router from 'vue-router'
import { env } from '@/core/js/env'
import MainComponent from '@/lazyout/Main.vue'
import SecondComponent from '@/lazyout/ZoneMain.vue'
// import store from "@/store"
// import { getToken } from "@/utils/auth" // getToken from cookie
// import { filterAsyncRouter } from "@/store/modules/permission"
let verifyLogin = (to, from, next) => {
  let uid = env.getUser().uid
  if (uid) {
    let vm = env.getVm()
    // next()
    vm.$dua.isLogin().then(res => {
      if (res) {
        next()
      } else {
        env.setUser({ uid: '' })
        router.push({ name: 'login' })
      }
    })
  } else {
    // next()
    router.push({ name: 'login' })
  }
}

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
// const originalPush = Router.prototype.push
// Router.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// }
Vue.use(Router)

// 不需要权限验证的页面
export const constantRoutes = [
  {
    path: '*',
    hidden: true,
    redirect: '/passport'
    // component: () => import('@/views/not-found/NotFound.vue')
  },
  {
    path: '/test',
    name: 'test',
    hidden: true,
    component: SecondComponent,
    children: [
      {
        path: '/test/room',
        name: 'myroom',
        meta: { title: '我的房间', icon: 'el-icon-menu' },
        component: () => import('@/views/room/list.vue')
      },
      {
        path: '/test/usrob',
        name: 'usrob',
        meta: { title: '人员名单', icon: 'el-icon-menu' },
        component: () => import('@/views/usrob/list.vue')
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    hidden: true,
    component: () => import('@/views/404.vue')
  },
  {
    path: '/bdvx',
    name: 'bdvx',
    hidden: true,
    component: () => import('@/views/passport/bdvx.vue')
  },
  {
    path: '/weixinlogin',
    name: 'weixinlogin',
    hidden: true,
    component: () => import('@/views/passport/weixinlogin.vue')
  },
  {
    path: '/zone',
    name: 'zone',
    component: MainComponent,
    children: [
      {
        path: '',
        name: 'zone-list',
        meta: { title: '账户管理', icon: 'el-icon-menu', fixed: true },
        component: () => import('@/views/zone/Zone.vue')
      }
    ]
  },
  {
    path: '/group',
    name: 'group',
    component: MainComponent,
    meta: { title: '店铺管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/group/group-list',
        name: 'group-list',
        meta: { title: '社区管理', icon: 'el-icon-document' },
        component: () => import('@/views/group/list/Group.vue')
      },
      {
        path: '/group/business-list',
        name: 'business-list',
        meta: { title: '企业管理', icon: 'el-icon-document' },
        component: () => import('@/views/business/index.vue')
      },
      {
        path: '/group/shop-list',
        name: 'shop-list',
        meta: { title: '店铺管理', icon: 'el-icon-document' },
        component: () => import('@/views/shop/index.vue')
      }
    ]
  },
  {
    path: '/apilog',
    name: 'apilog',
    component: MainComponent,
    meta: { title: '日志管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/apilog/list',
        name: 'group-list',
        meta: { title: '日志管理', icon: 'el-icon-document' },
        component: () => import('@/views/apilog/list/qrylog.vue')
      },
      {
        path: '/apilog/total',
        name: 'group-list',
        meta: { title: '日志统计', icon: 'el-icon-document' },
        component: () => import('@/views/apilog/logstt/qrylogstt.vue')
      }
    ]
  },
  {
    path: '/authority',
    name: 'authority',
    component: MainComponent,
    meta: { title: '角权管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/authority/root',
        name: 'root',
        meta: { title: '权限管理', icon: 'el-icon-document' },
        component: () => import('@/views/authority/power/index.vue')
      },
      {
        path: '/authority/role',
        name: 'role',
        meta: { title: '角色管理', icon: 'el-icon-document' },
        component: () => import('@/views/authority/role/index.vue')
      },
      {
        path: '/authority/angularpower',
        name: 'angularpower',
        meta: { title: '角权管理', icon: 'el-icon-document' },
        component: () => import('@/views/authority/angularpower/index.vue')
      },
      {
        path: '/authority/empower',
        name: 'empower',
        meta: { title: '授权管理', icon: 'el-icon-document' },
        component: () => import('@/views/authority/empower/index.vue')
      }
    ]
  },
  {
    path: '/court',
    name: 'court',
    component: MainComponent,
    meta: { title: '小区管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/court/province',
        name: 'province',
        meta: { title: '省区管理', icon: 'el-icon-document' },
        component: () => import('@/views/court/province/index.vue')
      },
      {
        path: '/court/city',
        name: 'city',
        meta: { title: '市区管理', icon: 'el-icon-document' },
        component: () => import('@/views/court/city/index.vue')
      },
      {
        path: '/court/area',
        name: 'area',
        meta: { title: '城区管理', icon: 'el-icon-document' },
        component: () => import('@/views/court/area/index.vue')
      },
      {
        path: '/court/court',
        name: 'court',
        meta: { title: '小区管理', icon: 'el-icon-document' },
        component: () => import('@/views/court/court/index.vue')
      }
    ]
  },
  {
    path: '/datamange',
    name: 'datamange',
    component: MainComponent,
    meta: { title: '数据管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/datamange/date',
        name: 'date',
        meta: { title: '日历管理', icon: 'el-icon-document' },
        component: () => import('@/views/data_mange/date/index.vue')
      },
      {
        path: '/datamange/other',
        name: 'other',
        meta: { title: '接口调试', icon: 'el-icon-document' },
        component: () => import('@/views/apitest/index.vue')
      }
    ]
  },
  {
    path: '/roommange',
    name: 'roommange',
    component: MainComponent,
    meta: { title: '房间管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/roommange/build',
        name: 'build',
        meta: { title: '楼栋管理', icon: 'el-icon-document' },
        component: () => import('@/views/roommange/build/index.vue')
      },
      {
        path: '/roommange/home',
        name: 'home',
        meta: { title: '套房管理', icon: 'el-icon-document' },
        component: () => import('@/views/roommange/home/index.vue')
      },
      {
        path: '/roommange/room',
        name: 'room',
        meta: { title: '房间管理', icon: 'el-icon-document' },
        component: () => import('@/views/roommange/room/index.vue')
      },
      {
        path: '/roommange/bed',
        name: 'bed',
        meta: { title: '床位管理', icon: 'el-icon-document' },
        component: () => import('@/views/roommange/bed/index.vue')
      },
      {
        path: '/roommange/rood',
        name: 'rood',
        meta: { title: '房屋管理', icon: 'el-icon-document' },
        component: () => import('@/views/roommange/rood/index.vue')
      }
    ]
  },
  {
    path: '/device-password',
    name: 'device-password',
    hidden: true,
    component: MainComponent,
    children: [
      {
        path: '',
        name: 'device-password-list',
        meta: { title: '我的社区', icon: 'el-icon-document' },
        component: () => import('@/views/device-password/index')
      }
    ]
  },
  {
    path: '/role/:ugrp_id',
    name: 'Role',
    component: MainComponent,
    hidden: true,
    children: [
      {
        path: '',
        name: 'role-list',
        meta: { title: '角色管理', icon: 'el-icon-document' },
        component: () => import('@/views/group/role/Role.vue')
      }
    ]
  },
  {
    path: '/usro/:role_id',
    name: 'Auth',
    component: MainComponent,
    hidden: true,
    children: [
      {
        path: '',
        name: 'usro-list',
        meta: { title: '授权管理', icon: 'el-icon-document' },
        component: () => import('@/views/group/auth/Auth.vue')
      }
    ]
  },
  {
    path: '/apm',
    name: 'apm',
    component: MainComponent,
    meta: { title: '应用管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/apm-list',
        name: 'apm-list',
        meta: { title: '应用管理', icon: 'el-icon-setting' },
        component: () => import('@/views/apm/list/Apm.vue')
      },
      {
        path: '/token',
        name: 'token-list',
        meta: { title: '令牌管理', icon: 'el-icon-setting' },
        component: () => import('@/views/apm/token/Token.vue')
      }
    ]
  },
  {
    path: '/obj',
    name: 'obj',
    component: MainComponent,
    meta: { title: '对象管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/obj/obj',
        name: 'obj',
        meta: { title: '对象管理', icon: 'el-icon-setting' },
        component: () => import('@/views/obj/obj/index.vue')
      },
      {
        path: '/obj/objc',
        name: 'objc',
        meta: { title: '象目管理', icon: 'el-icon-setting' },
        component: () => import('@/views/obj/objc/index.vue')
      },
      {
        path: '/obj/objr',
        name: 'objr',
        meta: { title: '象权管理', icon: 'el-icon-setting' },
        component: () => import('@/views/obj/objr/index.vue')
      },
      {
        path: '/obj/obje',
        name: 'obje',
        meta: { title: '对象异常', icon: 'el-icon-setting' },
        component: () => import('@/views/obj/obje/index.vue')
      },
      {
        path: '/obj/objl',
        name: 'objl',
        meta: { title: '对象日志', icon: 'el-icon-setting' },
        component: () => import('@/views/obj/objl/index.vue')
      }
    ]
  },
  {
    path: '/gwenv',
    name: 'gwenv',
    component: MainComponent,
    meta: { title: '网关管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/gwenv/dvlog-list',
        name: 'dvlog-list',
        meta: { title: '设备日志', icon: 'el-icon-printer' },
        component: () => import('@/views/dvlog/list.vue')
      },
      {
        path: '/gwenv/gwlog-list',
        name: 'gwlog-list',
        meta: { title: '网关日志', icon: 'el-icon-setting' },
        component: () => import('@/views/gwlog/list.vue')
      },
      {
        path: '/gwenv/gwenv-list',
        name: 'gwenv-list',
        meta: { title: '网管环境', icon: 'el-icon-printer' },
        component: () => import('@/views/gwenv/list.vue')
      },
      {
        path: '/gwenv/bluetooth-gateway',
        name: 'bluetooth-gateway',
        meta: { title: '蓝牙网关', icon: 'el-icon-document' },
        component: () => import('@/views/bluetooth-gateway/list.vue')
      },
      {
        path: '/gwenv/iBeacon',
        name: 'iBeacon',
        meta: { title: 'iBeacon', icon: 'el-icon-document' },
        component: () => import('@/views/iBeacon/list.vue')
      }
    ]
  },
  {
    path: '/device',
    name: 'device',
    component: MainComponent,
    meta: { title: '设备管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/device/belt-list',
        name: 'belt-list',
        meta: { title: '我的设备', icon: 'el-icon-printer' },
        component: () => import('@/views/belt/list.vue')
      },
      {
        path: '/device/products-list',
        name: 'products-list',
        meta: { title: '我的产品', icon: 'el-icon-document' },
        component: () => import('@/views/products/list.vue')
      },
      {
        path: '/device/device-password',
        name: 'device-password',
        meta: { title: '设备密码', icon: 'el-icon-printer' },
        component: () => import('@/views/device-password/index')
      },
      {
        path: '/device/dev-list',
        name: 'dev-list',
        meta: { title: '设备异常', icon: 'el-icon-document' },
        component: () => import('@/views/deve/list.vue')
      },
      {
        path: '/device/dvlog-list-new',
        name: 'dvlog-list-new',
        meta: { title: '设备日志', icon: 'el-icon-document' },
        component: () => import('@/views/devl/list.vue')
      },
      {
        path: '/device/sleep-chart',
        name: 'sleep-chart',
        meta: { title: '绵绵床带', icon: 'el-icon-document' },
        component: () => import('@/views/sleep-chart/list.vue')
      },
      {
        path: '/device/blood-oxygen-list',
        name: 'blood-oxygen-list',
        meta: { title: '血氧仪', icon: 'el-icon-document' },
        component: () => import('@/views/blood-oxygen/list.vue')
      },
      {
        path: '/device/oxygen-chart-list',
        name: 'oxygen-chart-list',
        meta: { title: '血氧图', icon: 'el-icon-document' },
        component: () => import('@/views/oxygen-chart/list.vue')
      },
      {
        path: '/device/enviroment-list',
        name: 'enviroment-list',
        meta: { title: '环境监测', icon: 'el-icon-document' },
        component: () => import('@/views/enviroment/list.vue')
      }
    ]
  },
  {
    path: '/beltmanagement',
    name: 'beltmanagement',
    component: MainComponent,
    meta: { title: '床带管理', icon: 'el-icon-document' },
    children: [
      {
        path: '/beltmanagement/cardiogram-list',
        name: 'cardiogram-list',
        meta: { title: '心冲击带', icon: 'el-icon-document' },
        component: () => import('@/views/cardiogram/list.vue')
      },
      {
        path: '/beltmanagement/sleepbib-list',
        name: 'sleepbib-list',
        meta: { title: '床带状态', icon: 'el-icon-document' },
        component: () => import('@/views/sleepbib/list.vue')
      },
      {
        path: '/beltmanagement/bcgwshp-list',
        name: 'bcgwshp-list',
        meta: { title: '心冲形状', icon: 'el-icon-document' },
        component: () => import('@/views/bcgwshp/list.vue')
      },
      {
        path: '/beltmanagement/hrvsim-list',
        name: 'hrvsim-list',
        meta: { title: 'HRV症状', icon: 'el-icon-document' },
        component: () => import('@/views/hrvsim/list.vue')
      },
      {
        path: '/beltmanagement/bcghrv-list',
        name: 'bcghrv-list',
        meta: { title: 'HRV标量', icon: 'el-icon-document' },
        component: () => import('@/views/bcghrv/list.vue')
      }
    ]
  },
  {
    path: '/heat-map',
    name: 'heat-map',
    component: MainComponent,
    children: [
      {
        path: '',
        name: 'heat-map-list',
        meta: { title: '热图界面', icon: 'el-icon-printer' },
        component: () => import('@/views/heat-map/list.vue')
      }
    ]
  },
  {
    path: '/user',
    name: 'UserInfo',
    component: MainComponent,
    hidden: true,
    children: [
      {
        path: '',
        name: 'user-list',
        meta: { title: '个人中心' },
        component: () => import('@/views/user/User.vue')
      }
    ]
  },
  {
    path: '/h5login',
    name: 'h5login',
    component: () => import('@/views/h5-login/index.vue'),
    hidden: true
  },
  {
    path: '/passport',
    component: () => import('@/lazyout/Passport.vue'),
    hidden: true,
    children: [
      {
        path: '',
        name: '',
        redirect: { name: 'login' }
      },
      {
        path: 'login',
        name: 'login',
        component: () => import('@/views/passport/Login.vue')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import('@/views/passport/Register.vue')
      },
      {
        path: 'contact',
        name: 'contact',
        component: () => import('@/views/passport/Contact.vue')
      }
    ]
  },
  {
    path: '/data',
    name: 'data',
    component: MainComponent,
    hidden: true,
    children: [
      {
        path: '',
        name: 'data-list',
        meta: { title: '我的数据', icon: 'el-icon-date' },
        component: () => import('@/views/data/list.vue')
      }
    ]
  },
  {
    path: '/file',
    name: 'file',
    component: MainComponent,
    hidden: true,
    children: [
      {
        path: '',
        name: 'file-list',
        meta: { title: '我的文件', icon: 'el-icon-document' },
        component: () => import('@/views/file/list.vue')
      }
    ]
  },
  {
    path: '/iotpass',
    name: 'iotpass-list',
    component: MainComponent,
    hidden: true,
    children: [
      {
        path: '',
        name: 'iotpass-list',
        meta: { title: '网关演示', icon: 'el-icon-printer' },
        component: () => import('@/views/iotpass/list.vue')
      }
    ]
  }
]

export const secondRoutes = [
]
// 需要根据用户权限进行验证加载的页面
// export const asyncRoutes = [
//   {
//     path: '/permission',
//     component: MainComponent,
//     redirect: '/permission/page',
//     alwaysShow: true, // will always show the root menu
//     name: 'Permission',
//     meta: {
//       title: 'Permission',
//       icon: 'lock'
//     },
//     children: [
//       {
//         path: 'page',
//         component: () => import('@/views/permission/page'),
//         name: 'PagePermission',
//         meta: {
//           title: 'Page Permission',
//           roles: ['admin'] // or you can only set roles in sub nav
//         }
//       },
//       {
//         path: 'directive',
//         component: () => import('@/views/permission/directive'),
//         name: 'DirectivePermission',
//         meta: {
//           title: 'Directive Permission'
//           // if do not set roles, means: this page does not require permission
//         }
//       },
//       {
//         path: 'role',
//         component: () => import('@/views/permission/role'),
//         name: 'RolePermission',
//         meta: {
//           title: 'Role Permission',
//           roles: ['admin']
//         }
//       }
//     ]
//   }
// ]
// const router = new Router({
//   mode: 'hash',
//   // base: process.env.BASE_URL,
//   base: '/',
//   routes: constantRoutes
// })
const createRouter = () =>
  new Router({
    mode: "hash",
    base: '/',
    routes: constantRoutes
  })
const router = createRouter()
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export default router
