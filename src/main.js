import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import './plugins/element.js'
import md5 from 'js-md5'
// import * as OSS from 'ali-oss'
import './core/scss/ui.scss'
import { env } from '@/core/js/env'
import store from './store'
const XduaClient = require('lovearth-xdua-browser-sdk')

Vue.config.productionTip = false

async function initDua() {
  const xduaClient = await XduaClient({
    LOGIN_TOKEN: "eyJhbGciOiJSUzI1NiIsImtpZCI6ImI1MGIxZWZmZGMwMzVlMjg2OWI2YzQ1ZjMzYmRmNWQ3In0.eyJpYXQiOjE2MjU2NDM3MTAsIm5iZiI6MTYyNTY0MzcxMCwiaXNzIjoieGR1YS5jb20iLCJleHAiOjE2NTcxNzk3MTAsImF1ZCI6IkNvbnNvbGVYIiwic3ViIjoiQW5vTnltdVMiLCJqdGkiOiIxMjM0NTY3OCIsImlwbSI6IioiLCJkdm0iOiIqIiwic2FwIjoiU3ZLZXJuZUwiLCJhcGkiOiIqIiwiY2xyIjoiWCIsIm93biI6IkR0NW12cnRVIiwidGlkIjoiMW9VU1g3MTgiLCJidWciOiJERSIsImxnbiI6Ik51bGxOdWxsIiwidHlwIjoiTCIsInRhZyI6IlByaW1hcnkiLCJ6b25lIjoiWGRVYVhkdUEiLCJjb3JwIjoiWGRVYVhkdUEiLCJzaG9wIjoiWGRVYVhkdUEiLCJyb2xlIjoiKiIsInJ1bGUiOiIqIn0.QDKZ-iF0rS9yBMQZhixNE-5q1lK_JJtWM2GqBEGKhhH3zE72wpr0P9YARrm290yAbpDsLUhrdMgR3NYaxvaukbDNO4tbayuiXXiS2IKUOmpkb2-BYasHd2ow97HcmHvs1aI38cWjKGZfkL0VYqmr-cKCny3t016VgkNadrhelMYxWhApk3BMQ8qSKz1PDaMq8F2SCSFZovTN0DBFh0eRWg4kifSU6naCVyIrbfirW4pj06PcCmTzZGhHkqTg0hb2jJZ4aUVsZ0lr4_APw4Fi69VtmrjyM9xRtmimoX-9ZokDqXwzsSSeyHCmdt32q8Qw2wCjET0hKWX2arvid8L_iQ"
  
  })
  Vue.prototype.$dua = xduaClient
}

Vue.prototype.$md5 = md5
env.init()
/*eslint no-extend-native: ["error", { "exceptions": ["Date"] }]*/
Date.prototype.format = function(format) {
  var o = {
    "M+": this.getMonth() + 1, // month
    "d+": this.getDate(), // day
    "h+": this.getHours(), // hour
    "m+": this.getMinutes(), // minute
    "s+": this.getSeconds(), // second
    "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
    "S": this.getMilliseconds() // millisecond
  }

  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length))
  }

  for (var k in o) {
    if (new RegExp("(" + k + ")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length === 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length))
    }
  }
  return format
}

// let fetch_oss_info = async () => {
//   try {
//     let raw_res = await fetch('http://115.29.50.216:3000')
//     let res = await raw_res.json()
//     let client = new OSS({
//       region: 'oss-cn-qingdao',
//       accessKeyId: res.AccessKeyId,
//       accessKeySecret: res.AccessKeySecret,
//       bucket: 'file-xdua-com'
//     })
//     Vue.prototype.$oss = client
//   } catch (e) {
//     console.log(e)
//   }
// }
// fetch_oss_info()

initDua().then(() => {
  let vm = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
  env.setVm(vm)
})

