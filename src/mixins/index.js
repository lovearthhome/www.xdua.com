export const utils = {
  methods: {
    // 深拷贝
    deepCopy(obj) {
      var newobj = obj.constructor === Array ? [] : {}
      let vm = this
      if (typeof obj !== "object") {
        return obj
      }
      for (var attr in obj) {
        newobj[attr] = vm.deepCopy(obj[attr])
      }
      return newobj
    },
    // 数据处理
    dealWithData(obj) {
      this.Data = this.deepCopy(obj)
      this.Data.forEach((item, index) => {
        if (item.state === 0) {
          item.state = {
            state: 0,
            label: '正常'
          }
        } else if (item.state === 1) {
          item.state = {
            state: 1,
            label: '冻结'
          }
        } else if (item.state === 2) {
          item.state = {
            state: 2,
            label: '下线'
          }
        } else if (item.state === 3) {
          item.state = {
            state: 3,
            label: '删除'
          }
        }
      })
    },
    // 显示页面弹窗
    showDialog(dialogType, refForm, form = {}) {
      this.dialogType = dialogType
      if (this.$refs[refForm]) {
        this.$refs[refForm].initForm(form)
      }
      this.isShow = true
    },
    // 打开弹框
    openDialog(value, e) {
      let vm = this
      vm.labelname = value.name
      vm.status = e.target.innerText
      vm.dialogTableVisible = true
      vm.id = value.id
      vm.stato = ''
    },
    // dialog 取消按钮
    cancleClick() {
      let vm = this
      vm.dialogTableVisible = false
      vm.labelname = ''
      vm.status = ''
      vm.stato = ''
      vm.id = ''
    },
    // 展示更多json
    showMoreJson(obj) {
      this.showMore = true
      this.moreJson = obj
    },
    notify(tl, is_suc = true, msg = "") {
      let vm = this
      vm.$notify({
        title: tl,
        message: msg,
        type: is_suc ? "success" : "error",
        offset: 100
      })
    },
    trim(str) {
      return str.replace(/^\s+|\s+$/g, "")
    }
  }
}
