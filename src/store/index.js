import Vue from 'vue'
import Vuex from 'vuex'
import getters from "./getter"
import app from "./modules/app"
import permission from './modules/permission'
import settings from "./modules/setting"
import tagsView from "./modules/tagsview"
import userInfo from "./modules/user"
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    app,
    permission,
    settings,
    tagsView,
    userInfo
  },
  getters
})

export default store
