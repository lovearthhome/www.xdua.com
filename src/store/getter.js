const getters = {
  sidebar: state => state.app.sidebar,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters,
  visitedViews: state => state.tagsView.visitedViews,
  userInfo: state => state.userInfo.userInfo,
  permission_comrouter: state => state.permission.comrouter
}
export default getters
