const state = {
  userInfo: []
}
const mutations = {
  UPDER_USER_INFO: (state, userInfo) => {
    state.userInfo = userInfo
  }
}
const actions = {
  updateUser({ commit }, userInfo) {
    commit('UPDER_USER_INFO', userInfo)
  }
}
export default {
  namespaced: true, // namespaced这是属性用于解决不同模块的命名冲突问题
  state,
  mutations,
  actions
}
