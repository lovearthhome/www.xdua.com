import { constantRoutes } from "@/router/index"
import Layout from "@/lazyout/Main.vue"
import { ComponentsRouter } from "@/router/modules/components.js"
const permission = {
  state: {
    routers: constantRoutes,
    addRouters: [],
    comrouter: ComponentsRouter
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRoutes.concat(routers)
    }
  },
  actions: {
    GenerateRoutes({ commit }, asyncRouter) {
      commit("SET_ROUTERS", asyncRouter)
    }
  }
}

export const filterAsyncRouter = routers => {
  // 遍历后台传来的路由字符串，转换为组件对象
  const accessedRouters = routers.filter(router => {
    if (router.component && router.length === 0) {
      router.children = null
    }
    if (router.component) {
      if (router.component === "Layout") {
        // Layout组件特殊处理
        router.component = Layout
      } else {
        const component = router.component
        router.component = loadView(component)
      }
    }
    if (router.children && router.children.length) {
      router.children = filterAsyncRouter(router.children)
    }
    return true
  })
  // console.log(accessedRouters);
  return accessedRouters
}

export const loadView = view => {
  // 路由懒加载
  return () => import(`@/views/${view}`)
}

export default permission
